var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var songSchema = new Schema({
    title: String,
    artist: String,
    file: String,
    groupID: String
})

module.exports = mongoose.model('Song', songSchema);
