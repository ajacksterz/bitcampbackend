var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var groupSchema = new mongoose.Schema({
    createdBy: String,
    roomName: String,
    roomPass: String
})

module.exports = mongoose.model('Group', groupSchema);