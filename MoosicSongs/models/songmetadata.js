var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var SongMetaData = new Schema({
	songName: String,
	artist: String,
	filename: String	
}, { collection : 'songmetadata' });

module.exports = mongoose.model('SongMetaData', SongMetaData);
