var express = require('express');
var router = express.Router();
var app = express();

var Song = require('../models/song');
var Group = require('../models/group');
var SongMetaData = require('../models/songmetadata');

var fs = require('fs');
var io = require('socket.io');
var path = require('path');
var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

router.post('/makeAGroup', function(req, res) {
    var groupInfo = new Group({
        createdBy: req.body.creator,
        roomName: req.body.roomName,
        roomPass: req.body.roomPassword
    })
    
    groupInfo.save(function(err) {
                if(err){
                    return handleError(err);
                }else{
                    console.log("Saved Successfully");   
                }
            })
    res.send(groupInfo);
})

router.post('/getGroupID', function(req, res) {
      Group.findOne({ 'createdBy' : req.body.username }, function(err, group) {
          console.log(req.body.username);
            if(err) {
                return handleError(err);
            }
            if(group) {
                var groupInfo = {
                    groupID : group._id
                }
                res.send(groupInfo);
            }
      })
})

router.get('/getAllGroups', function(req, res) {
    Group.find({}, function(err, groups) {
        res.send(groups);
    })
})

router.post('/uploadSong', function(req, res) {
    var songInfo = new Song({
        title: req.body.title,
        artist: req.body.artist,
        file: req.body.file,
        groupID: req.body.groupID
    })
    songInfo.save(function(err) {
            if(err) throw err;
	    res.send("Success");
    })
})

router.get('/getGroupSongs/:groupID', function(req, res) {
    var groupID = req.params.groupID;
    
    Song.find({ 'groupID' : groupID }, function(err, songs) {
        if(err) return handleError(err);
        
        if(songs) {
           res.send(songs);    
        }
    })
})

router.get('/getSongFile/:filename', function(req, res) {
	var fileName = req.params.filename;
	var pathToFile = path.join('/home/ajackster/Moosic/MoosicSongs/files/', fileName);
	var stat = fs.statSync(pathToFile);
	res.writeHead(200, {
		'Content-Type': 'audio/mpeg',
		'Content-Length': stat.size
	});
	var readStream = fs.createReadStream(pathToFile);
	readStream.pipe(res);
})

router.get('/getListOfSongs', function(req, res) {
	SongMetaData.find({}, function(err, metadata) {
		res.send(metadata);
	})
})

router.get('/deleteSong/:groupID/:songTitle', function(req, res) {
	Song.find({ 'groupID': req.params.groupID,
		    'title': req.params.songTitle
		  }, function(err, song) {
			if(err) throw err;
			
			song.remove().exec();
		 })
})

module.exports = router;
