var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');
var bcrypt = require('bcrypt');
var bodyParser = require('body-parser');
var express = require('express');
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

module.exports = function(passport) {
    passport.use('signup', new LocalStrategy({
        passReqToCallback : true
    },
    function(req, username, password, done) {
        
        findOrCreateUser = function(){
            User.findOne({'username':username}, function(err, user) {
                
                //for case of any error return
                if(err) {
                    console.log('Error in Signup: ' + err);
                    return done(err);
                }
                //this means that there is already a user with the input username
                if(user) {
                       console.log('User already exists');
                       return done(null, false, req.flash('message', 'User already exists'));
                }else{
                 //if no user with the same username then create the user
                    
                var newUser = new User();
                
                newUser.username = username;
                newUser.password = createHash(password, 10);
                newUser.email = req.body.email;
                newUser.firstName = req.body.firstName;
                newUser.lastName = req.body.lastName;
                    
                //save the user
                newUser.save(function(err) {
                    if(err) {
                        console.log('Error in saving the user: ' + err);
                        throw err;
                    }
                    console.log('User Registration successful');
                    return done(null, newUser);
                })
                }
            })
        }
        //execut findOrCreateUser in the next tick of the event loop
        process.nextTick(findOrCreateUser);
        
        })
        
)
    var createHash = function(password) {
        return bcrypt.hashSync(password, bcrypt.genSaltSync(10));
    }
}