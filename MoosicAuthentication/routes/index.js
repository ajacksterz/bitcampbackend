var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var fs = require('fs');
var User = require('../models/user');

var isAuthenticated = function(req, res, next) {
    if(req.isAuthenticated())
        return next();
    
    res.redirect('/');
}

module.exports = function(passport) {
    router.get('/', function(req, res) {
        res.render('index', {message: req.flash('message')});     
    })
    
    router.get('/home', isAuthenticated, function(req, res) {
        res.render('home', { user: req.user });  
    })
    
    router.get('/signup', function(req, res) {
        res.render('register', {message: req.flash('message')});
    })
    
    router.post('/login', function(req, res, next) {
        passport.authenticate('login', function(err, user, info) {
            if(err){
                return next(err);
            }
            if(!user) {
                res.send("fail");
            }
            if(user) {
                res.send("success");
            }
        })(req, res, next);
    })
                
    router.post('/signup', function(req, res, next) {
        passport.authenticate('signup', function(err, user, info) {
            if(err){
                return next(err);
            }
            if(!user) {
                res.send("fail");   
            }else{
                res.send("success");   
            }
        })(req, res, next);
    })
    
    router.get('/getUserInfo/:username', function(req, res) {
        var username = req.params.username;
        User.findOne({ 'username' : username }, function(err, user) {
            if(err) return handleError(err);
            
            var userInfo = {
                username: user.username,
                firstName: user.firstName,
                lastName: user.lastName,
                email: user.email,
                id: user._id
            }
            res.send(userInfo);
        })
    })
    
    router.get('/signout', function(req, res) {
        req.logout();
        res.redirect('/');
    })
    
    return router;
};
